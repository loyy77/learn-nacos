示例代码位置：

配置管理：`com/shixihub/learn/nacos/NacosConfigTest.java`  
服务管理：`com/shixihub/learn/nacos/NacosServiceTest.java`



# 下载

`wget https://github.com/alibaba/nacos/releases/download/2.0.2/nacos-server-2.0.2.zip`

# 解压

```
unzip nacos-server-2.0.2.zip
cd nacos/bin

```

# 启动服务

```
root@shixihub:~/nacos/bin# bash startup.sh -m standalone
/usr/lib/jvm/java-8-openjdk-amd64/bin/java  -Xms512m -Xmx512m -Xmn256m -Dnacos.standalone=true -Dnacos.member.list= -Djava.ext.dirs=/usr/lib/jvm/java-8-openjdk-amd64/jre/lib/ext:/usr/lib/jvm/java-8-openjdk-amd64/lib/ext -Xloggc:/root/nacos/logs/nacos_gc.log -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:+PrintGCTimeStamps -XX:+UseGCLogFileRotation -XX:NumberOfGCLogFiles=10 -XX:GCLogFileSize=100M -Dloader.path=/root/nacos/plugins/health,/root/nacos/plugins/cmdb -Dnacos.home=/root/nacos -jar /root/nacos/target/nacos-server.jar  --spring.config.additional-location=file:/root/nacos/conf/ --logging.config=/root/nacos/conf/nacos-logback.xml --server.max-http-header-size=524288
nacos is starting with standalone
nacos is starting，you can check the /root/nacos/logs/start.out
root@shixihub:~/nacos/bin# 
```
