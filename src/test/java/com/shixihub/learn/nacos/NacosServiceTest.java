package com.shixihub.learn.nacos;

import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.listener.Event;
import com.alibaba.nacos.api.naming.listener.EventListener;
import com.alibaba.nacos.api.naming.listener.NamingEvent;
import com.alibaba.nacos.api.naming.pojo.Instance;
import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务发现
 *
 * @author LiShixi 2021/7/16 3:22 下午
 */
public class NacosServiceTest {
    
    static final String SERVE_ADDR = "http://127.0.0.1:8848";
    
    @Test
    public void testRegistryService() {
        try {
            NamingService naming = NamingFactory.createNamingService(SERVE_ADDR);
            
            final String serviceName = "nacos.test.ppss.web";
            final String groupName = "Nacos-Test";
            
            
            final Thread test1 = new Thread(() -> {
                int count = 1;
                while (count++ > 0) {
                    // 这种注册的实例是瞬时的，不心跳就删掉了。
                    try {
                        naming.registerInstance(serviceName, groupName, "127.0.0.1", 20071, "TEST1");
                        System.out.println("注册服务 = " + LocalDateTime.now());
                        Thread.sleep(5 * 1000);
                    } catch (NacosException | InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }, "reg service");
            test1.setDaemon(true);
            test1.start();
            
            
            final String serviceName1 = "nacos.test.ppss.web2";
            //查找一个健康实例
            final Instance aDefault = naming.selectOneHealthyInstance(serviceName1, groupName, Lists.newArrayList("DEFAULT"), true);
            System.out.println("aDefault = " + aDefault);
            
            // 注销所有实例
            final List<Instance> test11 = naming.getAllInstances(serviceName1, groupName, Lists.newArrayList("DEFAULT"));
            for (Instance instance : test11) {
                System.out.println("instance = " + test11);
                naming.deregisterInstance(serviceName1, groupName, instance.getIp(), instance.getPort(), "TEST1");
            }
            
            final Thread add_instance = new Thread(() -> {
                int count = 1;
                while (count++ < 30) {
                    // 注册更多配置的服务
                    Instance instance = new Instance();
                    instance.setIp("127.0.0.1");
                    instance.setPort(4545 + count);
                    instance.setHealthy(true);
                    instance.setWeight(2.0);
                    instance.setEphemeral(false);
                    Map<String, String> instanceMeta = new HashMap<>();
                    instanceMeta.put("site", "et2");
                    instance.setMetadata(instanceMeta);
                    try {
                        System.out.println("instance = " + instance);
                        naming.registerInstance(serviceName1, groupName, instance);
                    } catch (NacosException e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                
            }, "add instance");
            add_instance.setDaemon(true);
            add_instance.start();
            
            
            // 获取健康实例
            System.out.println("健康实例 = " + naming.selectOneHealthyInstance(serviceName, groupName));
            
            // 订阅服务下的实例列表，当serverName下的instance列表变化就会被通知到
            naming.subscribe("nacos.test.ppss.web2", groupName, Lists.newArrayList("TEST1"), new EventListener() {
                @Override
                public void onEvent(Event event) {
                    if (event instanceof NamingEvent) {
                        System.out.println(((NamingEvent) event).getServiceName());
                        System.out.println(((NamingEvent) event).getInstances());
                    }
                    
                }
            });
            
            // 程序不退出，守护线程还有活要干。
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (NacosException e) {
            e.printStackTrace();
        }
    }
    
}
