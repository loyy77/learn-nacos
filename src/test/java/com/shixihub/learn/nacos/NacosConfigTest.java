package com.shixihub.learn.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * 配置管理
 *
 * @author LiShixi 2021/7/16 2:36 下午
 */
class NacosConfigTest {
    
    public static final String DATA_ID = "com.shixihub.learn.nacos.testGetConfig";
    public static final String GROUP = "Nacos-Test";
    public static final String SERVER_ADDR = "http://127.0.0.1:8848";
    Logger log = LoggerFactory.getLogger(NacosConfigTest.class);
    
    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }
    
    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }
    
    
    @Test
    public void testGetConfig() {
        try {
            Properties properties = new Properties();
            properties.put("serverAddr", SERVER_ADDR);
            ConfigService configService = NacosFactory.createConfigService(properties);
            String content = configService.getConfig(DATA_ID, GROUP, 5000);
            System.out.println(content);
        } catch (NacosException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    @Test
    public void testPutConfig() {
        try {
            // 初始化配置服务，控制台通过示例代码自动获取下面参数
            // 重复publish会更新
            Properties properties = new Properties();
            properties.put("serverAddr", SERVER_ADDR);
            ConfigService configService = NacosFactory.createConfigService(properties);
            boolean isPublishOk = configService.publishConfig(DATA_ID, GROUP, "这都存都啥啥啥！" + LocalDateTime.now());
            System.out.println(isPublishOk);
        } catch (NacosException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    @Test
    public void testDeleteConfig() {
        try {
            // 初始化配置服务，控制台通过示例代码自动获取下面参数
            Properties properties = new Properties();
            properties.put("serverAddr", SERVER_ADDR);
            
            ConfigService configService = NacosFactory.createConfigService(properties);
            boolean isRemoveOk = configService.removeConfig(DATA_ID, GROUP);
            System.out.println(isRemoveOk);
        } catch (NacosException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    @Test
    public void testAddListener() {
        try {
            
            Properties properties = new Properties();
            properties.put("serverAddr", SERVER_ADDR);
            ConfigService configService = NacosFactory.createConfigService(properties);
            String content = configService.getConfig(DATA_ID, GROUP, 5000);
            System.out.println(content);
            final Listener listener = new Listener() {
                @Override
                public void receiveConfigInfo(String configInfo) {
                    System.out.println("recieve:" + configInfo);
                }
                
                @Override
                public Executor getExecutor() {
                    return null;
                }
            };
            configService.addListener(DATA_ID, GROUP, listener);
            
            // 测试让主线程不退出，因为订阅配置是守护线程，主线程退出守护线程就会退出。 正式代码中无需下面代码
            int count = 7;
            while (true) {
                try {
                    Thread.sleep(1000);
                    // 模拟改配置，好让上面监听器监测到消息
                    final boolean b = configService.publishConfig(DATA_ID, GROUP, "配置XXX " + LocalDateTime.now());
                    System.out.println("发布配置" + b);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // 测试删除监听器
                if (count-- <= 0) {
                    configService.removeListener(DATA_ID, GROUP, listener);
                    System.out.println("删掉监控器" + listener);
                    break;
                }
            }
        } catch (NacosException e) {
            e.printStackTrace();
        }
    }
    
    @Test
    public void testDeleteListener(Listener yourListener) {
        try {
            Properties properties = new Properties();
            properties.put("serverAddr", SERVER_ADDR);
            ConfigService configService = NacosFactory.createConfigService(properties);
            configService.removeListener(DATA_ID, GROUP, yourListener);
        } catch (NacosException e) {
            e.printStackTrace();
        }
    }
}
